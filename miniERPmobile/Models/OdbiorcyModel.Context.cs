﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace miniERPmobile.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class w59018Entities : DbContext
    {
        public w59018Entities()
            : base("name=w59018Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<DetailsFlat> DetailsFlat { get; set; }
        public virtual DbSet<dokdost> dokdost { get; set; }
        public virtual DbSet<dokdostpoz> dokdostpoz { get; set; }
        public virtual DbSet<dokdostpoz_temp> dokdostpoz_temp { get; set; }
        public virtual DbSet<doksprzed> doksprzed { get; set; }
        public virtual DbSet<doksprzedpoz> doksprzedpoz { get; set; }
        public virtual DbSet<doksprzedpoz_temp> doksprzedpoz_temp { get; set; }
        public virtual DbSet<dostawca> dostawca { get; set; }
        public virtual DbSet<Flat> Flat { get; set; }
        public virtual DbSet<odbiorca> odbiorca { get; set; }
        public virtual DbSet<partmag> partmag { get; set; }
        public virtual DbSet<wytwor> wytwor { get; set; }
    }
}
