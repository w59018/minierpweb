﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using miniERPmobile.Models;

namespace miniERPmobile.Controllers
{
    public class odbiorcaController : Controller
    {
        private w59018Entities db = new w59018Entities();

        // GET: odbiorca
        public ActionResult Index()
        {
            return View(db.odbiorca.ToList());
        }

        // GET: odbiorca/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odbiorca odbiorca = db.odbiorca.Find(id);
            if (odbiorca == null)
            {
                return HttpNotFound();
            }
            return View(odbiorca);
        }

        // GET: odbiorca/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "odbiorca_id,nazwa,powiat,gmina,kodpoczt,miasto,poczta,ulica,nrdomu,rabat")] odbiorca odbiorca)
        {
            if (ModelState.IsValid)
            {
                db.odbiorca.Add(odbiorca);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(odbiorca);
        }

        // GET: odbiorca/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odbiorca odbiorca = db.odbiorca.Find(id);
            if (odbiorca == null)
            {
                return HttpNotFound();
            }
            return View(odbiorca);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "odbiorca_id,nazwa,powiat,gmina,kodpoczt,miasto,poczta,ulica,nrdomu,rabat")] odbiorca odbiorca)
        {
            odbiorca.rabat = Convert.ToDecimal(odbiorca.rabat);

            if (ModelState.IsValid)
            {
                db.Entry(odbiorca).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(odbiorca);
        }


        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odbiorca odbiorca = db.odbiorca.Find(id);
            if (odbiorca == null)
            {
                return HttpNotFound();
            }
            return View(odbiorca);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            odbiorca odbiorca = db.odbiorca.Find(id);
            db.odbiorca.Remove(odbiorca);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
